module.exports = function(obj){
    const res = []
    if( obj !== undefined){
        for(let key in obj){
            if(typeof(key) !== "function"){
                res.push(obj[key]);
            }
        }
    }
    return res;
}